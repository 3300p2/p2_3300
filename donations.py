import csv


years = ['2008','2012', '2016']

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def donorRead(DATA_YEAR):
    # populate mapping between state and it's list of occupations
    dems = ['Sanders, Bernard', 'Clinton, Hilary Rodham', 'Obama, Barack']
    reps =  ['Rubio, Marco', 'Cruz, Ted', 'Romney, Mitt', 'McCain, John S']
    candidates = dems + reps

    # states = ['AK', 'AL', 'AR', 'AU', 'AZ', 'BU', 'C', 'CA', 'CO', 'CT', 'DC', 'DE', 'EN', 'FL', 'FR', 'GA', 'HI', 'HO', 'IA', 'ID', 'IL', 'IN', 'IS', 'JA', 'KS', 'KY', 'LA', 'LE', 'LO', 'MA', 'MD', 'ME', 'MI', 'MN', 'MO', 'MS', 'MT', 'N.', 'NC', 'ND', 'NE', 'NH', 'NJ', 'NL', 'NM', 'NV', 'NY', 'OH', 'OK', 'OR', 'PA', 'RI', 'SC', 'SD', 'SI', 'SW', 'TN', 'TO', 'TX', 'U*', 'UT', 'VA', 'VT', 'WA', 'WI', 'WV', 'WY', 'YT']
    states = ["AK",
    "AL",
    "AR",
    "AZ",
    "CA",
    "CO",
    "CT",
    "DC",
    "DE",
    "FL",
    "GA",
    "HI",
    "IA",
    "ID",
    "IL",
    "IN",
    "KS",
    "KY",
    "LA",
    "MA",
    "MD",
    "ME",
    "MI",
    "MN",
    "MO",
    "MS",
    "MT",
    "NC",
    "ND",
    "NE",
    "NH",
    "NJ",
    "NM",
    "NV",
    "NY",
    "OH",
    "OK",
    "OR",
    "PA",
    "RI",
    "SC",
    "SD",
    "TN",
    "TX",
    "UT",
    "VA",
    "VT",
    "WA",
    "WI",
    "WV",
    "WY"]
    # print states
    statesDict = {}
    for state in states:
        statesDict[state] = {}

    occupationDict = dict()
    occupations = set()
    x = 0
    with open(DATA_YEAR + 'Donors.csv', 'rt') as f:
        reader = csv.reader(f)
        for row in reader:
            state = row[5]
            donation = row[9]
            candidate = row[2]

            if state in states and candidate in candidates:
                if candidate in dems:
                    party = 'Democrat'
                else:
                    party = 'Republican'

                if donation != 'contb_receipt_amt':
                    donation = float(donation)

                    if party not in statesDict[state]:
                        statesDict[state][party] = 0
                    statesDict[state][party] += donation


    # print statesDict
    StateDonations = []

    for state in states:
        if 'Democrat' not in statesDict[state]:
            demDonation = 0
        else:
            demDonation = statesDict[state]['Democrat']

        if 'Republican' not in statesDict[state]:
            repDonation = 0
        else:
            repDonation = statesDict[state]['Republican']

        delta = demDonation - repDonation
        donSum = demDonation + repDonation
        if donSum == 0:
            percent = 0
        else:
            percent = delta / (demDonation + repDonation) * 100

        StateDonations.append([demDonation, repDonation, percent])

    return StateDonations
   
# with open('ugh.csv', 'rt') as f:
#     SSS = ['AA', 'AE', 'AK', 'AL', 'AM', 'AP', 'AR', 'AS', 'AU', 'AZ', 'BC', 'BR', 'BU', 'C', 'CA', 'CO', 'CT', 'DC', 'DE', 'EN', 'FF', 'FL', 'FR', 'GA', 'GE', 'GU', 'HI', 'HO', 'IA', 'ID', 'IL', 'IN', 'IS', 'JA', 'KS', 'KY', 'LA', 'LE', 'LO', 'MA', 'MB', 'MD', 'ME', 'MI', 'MN', 'MO', 'MP', 'MS', 'MT', 'N.', 'NC', 'ND', 'NE', 'NH', 'NJ', 'NL', 'NM', 'NO', 'NS', 'NV', 'NY', 'OH', 'OK', 'ON', 'OR', 'PA', 'PR', 'QC', 'RI', 'SC', 'SD', 'SI', 'SO', 'SW', 'TE', 'TN', 'TO', 'TX', 'U*', 'UK', 'UT', 'VA', 'VI', 'VT', 'WA', 'WE', 'WI', 'WV', 'WY', 'XX', 'YT', 'ZZ']

#     reader = csv.reader(f)
#     for row in reader:
#         state = row[5]
#         if state in SSS:
#             i = SSS.index(state)
#             SSS.pop(i)

# print SSS



for year in years:
    donations = donorRead(year)
    print "Year: ",year, ", ", donations
# ### WRITE OUTPUT ###
# with open('DonationOccupationData' + DATA_YEAR + '.csv', 'wb') as outputFile:
# 	outputWriter = csv.writer(outputFile)
# 	outputWriter.writerow(sorted(occupations))

# 	# write data
# 	for state in states:
# 		sortedList = getValuesSortedByKeys(statesDict[state])
# 		outputWriter.writerow(sortedList)


