

//----------------------------------------------------------------------------
//http://javascript.tutorialhorizon.com/2015/03/05/creating-an-animated-ring-or-pie-chart-in-d3js/
var vPd_data = [
    {candidate: "Democrat", votes: "2178391", contribution: 50554857.369999886, ratio: 4.308964782665363},
    {candidate: "Republican", votes: "2270395", contribution: 41018210.690000005, ratio: 5.5350902972311}
];
var maxWidth = 1000;
var maxHeight = 1000;
var outerRadius = 300;
var ringWidth = 100;

// This function helps you figure out when all
// the elements have finished transitioning
// Reference: https://groups.google.com/d/msg/d3-js/WC_7Xi6VV50/j1HK0vIWI-EJ
function checkEndAll(transition, callback) {
    var n = 0;
    transition
    .each(function() { ++n; })
    .each("end", function() {
        if (!--n) callback.apply(this, arguments);
    });
}    

var vPd_svg;
function drawAnimatedRingChart(config) {
    var pie = d3.layout.pie().value(function (d) {
        return d.votes;
    });

    var color = d3.scale.category10();
    var arc = d3.svg.arc();

    // This function helps transition between
    // a starting point and an ending point
    // Also see: http://jsfiddle.net/Nw62g/3/
    function tweenPie(finish) {
        var start = {
                startAngle: 0,
                endAngle: 0
            };
        var i = d3.interpolate(start, finish);
        return function(d) { return arc(i(d)); };
    }
    arc.outerRadius(config.outerRadius || outerRadius)
        .innerRadius(config.innerRadius || innerRadius);

    // Remove the previous ring
    d3.select(config.el).selectAll('g').remove();

    vPd_svg = d3.select("#pieRing")
        .attr('width', maxWidth).attr('height', maxHeight);

    // Add the groups that will hold the arcs
    var groups = vPd_svg.selectAll('g.arc')
    .data(pie(config.data))
    .enter()
    .append('g')
    .attr({
        'class': 'arc',
        'transform': 'translate(' + outerRadius + ', ' + outerRadius + ')'
    });

    // Create the actual slices of the pie
    groups.append('path')
    .attr("stroke", "#f2f2f2")
    .attr("stroke-width", "5px")
    .attr({
        'fill': function (d, i) {
            if(vPd_data[i].candidate == 'Democrat') {
                return '#2e5cb8';
            }
            else {
                return '#e62e00'
            }
        }
    })
    .attr("class", function(d, i) {
        if(vPd_data[i].candidate == 'Democrat') {
            return 'arcPath Democrat';
        }
        else {
            return 'arcPath Republican';
        }
    })
    .transition()
    .duration(config.duration || 1000)
    .attrTween('d', tweenPie)
    .call(checkEndAll, function () {
        //put labels
    });
}

// Render the initial ring
drawAnimatedRingChart({
    el: 'vPd_svg',
    outerRadius: outerRadius,
    innerRadius: outerRadius - ringWidth,
    data: vPd_data
});

//--------------------------------------------------------------------------

var xScale = d3.scale.ordinal()
            .rangeRoundBands([0, maxWidth], .7)
            .domain(vPd_data.map(function(d) {return d.candidate}));

        var yScale = d3.scale.linear()
            .range([maxHeight, 0])
            .domain([0,d3.max(vPd_data, function(d){return d.ratio}) + 2]);

        //Create hover boxes to display data
        vPd_svg.selectAll('.hoverBox')
            .data(vPd_data)
            .enter().append("rect")
            .attr('class',function(d) { return d.candidate+"_hover hoverBox"})
            .attr("x", function(d) { return xScale(d.candidate); })
            .attr("width", 250)
            .attr("y", function(d) { return yScale(d.ratio) - maxHeight/4; })
            .attr("height", 100);

        var money_formatter = d3.format('$,.2f');
        var vote_formatter = d3.format(',');

        vPd_svg.selectAll('.hoverBox_text')
            .data(vPd_data)
            .enter().append("text")
            .attr('class',function(d) { return d.candidate+"_hoverText hoverBox_text"})
            .attr("x", function(d) { return xScale(d.candidate) ; })
            .attr("width", 200)
            .attr("y", function(d) { return yScale(d.ratio) - maxHeight/5; })
            .attr("height", 100)
            .append('svg:tspan')
            .attr('x', function(d) { return xScale(d.candidate) + maxWidth / 100; })
            .attr('dy', 5)
            .text(function(d) { return "Summary of " + d.candidate + " Party"})
            .append('svg:tspan')
            .attr('x', function(d) { return xScale(d.candidate) + maxWidth / 100; })
            .attr('dy', 20)
            .text(function(d) { return 'Votes: ' + vote_formatter(d.votes)})
            .append('svg:tspan')
            .attr('x', function(d) { return xScale(d.candidate) + maxWidth / 100; })
            .attr('dy', 20)
            .text(function(d) { return 'Contributions: ' + money_formatter(d.contribution)});

        d3.selectAll('.arcPath').on("mouseover", function() {
            d3.select(this).attr('opacity',0.5);

        }).on("mouseout", function() {
            d3.selectAll('.arcPath').attr('opacity', 1.0);
        });
        
        jQuery('.Democrat').hover(function(){
          jQuery('.Democrat_hover').css('fill','#fff');
          jQuery('.Democrat_hover').css('stroke','#000');
          jQuery('.Democrat_hoverText').css('display','block');
        }, function(){
          jQuery('.Democrat_hover').css('fill','none');
          jQuery('.Democrat_hover').css('stroke','none');
          jQuery('.Democrat_hoverText').css('display','none');
        });

        jQuery('.Republican').hover(function(){
          jQuery('.Republican_hover').css('fill','#fff');
          jQuery('.Republican_hover').css('stroke','#000');
          jQuery('.Republican_hoverText').css('display','block');
        }, function(){
          jQuery('.Republican_hover').css('fill','none');
          jQuery('.Republican_hover').css('stroke','none');
          jQuery('.Republican_hoverText').css('display','none');
        });


