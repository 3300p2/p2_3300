import csv
import sys
from collections import defaultdict
from collections import OrderedDict

DATA_YEAR = '2016'
states = ['AA', 'AE', 'AK', 'AL', 'AM', 'AP', 'AR', 'AS', 'AU', 'AZ', 'BC', 'BR', 'BU', 'C', 'CA', 'CO', 'CT', 'DC', 'DE', 'EN', 'FF', 'FL', 'FR', 'GA', 'GE', 'GU', 'HI', 'HO', 'IA', 'ID', 'IL', 'IN', 'IS', 'JA', 'KS', 'KY', 'LA', 'LE', 'LO', 'MA', 'MB', 'MD', 'ME', 'MI', 'MN', 'MO', 'MP', 'MS', 'MT', 'N.', 'NC', 'ND', 'NE', 'NH', 'NJ', 'NL', 'NM', 'NO', 'NS', 'NV', 'NY', 'OH', 'OK', 'ON', 'OR', 'PA', 'PR', 'QC', 'RI', 'SC', 'SD', 'SI', 'SO', 'SW', 'TE', 'TN', 'TO', 'TX', 'U*', 'UK', 'UT', 'VA', 'VI', 'VT', 'WA', 'WE', 'WI', 'WV', 'WY', 'XX', 'YT', 'ZZ']

# populate mapping between state and it's list of occupations
statesDict = {}
for state in states:
	statesDict[state] = defaultdict(int)

occupationDict = defaultdict(int)
occupations = set()

# -- http://stackoverflow.com/questions/354038/how-do-i-check-if-a-string-is-a-number-float-in-python
def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

# read csv file -- https://pymotw.com/2/csv/
with open(DATA_YEAR + 'Donors.csv', 'rt') as f:
    reader = csv.reader(f)
    for row in reader:
    	state = row[5]
    	occupation = row[8]
    	if occupation != 'contbr_occupation' and occupation.isalnum() and not is_number(occupation):
			statesDict[state][occupation] += 1    
			occupationDict[occupation] += 1	
			occupations.add(occupation.strip())

def printOccupationsFromState(statesDict, state, NUM_ITEMS_TO_PRINT=100):
	""" Prints the NUM_ITEMS_TO_PRINT occupations with the highest number of people who donated
	from that occupation from the given state. """
	NUM_ITEMS_PRINTED = 0
	for entry in sorted(statesDict[state].items(), key=lambda x: x[1], reverse=True):
		print entry
		
		NUM_ITEMS_PRINTED += 1

		if NUM_ITEMS_PRINTED >= NUM_ITEMS_TO_PRINT:
			return

# for state in states:
# 	print 
# 	print state
# 	printOccupationsFromState(statesDict, state, 10)

def getValuesSortedByKeys(occupationDict):
	l = []
	for occupation in sorted(occupations):
		l.append(occupationDict[occupation])
	return l


### WRITE OUTPUT ###
with open('stateOccupationData' + DATA_YEAR + '.csv', 'wb') as outputFile:
	outputWriter = csv.writer(outputFile)
	outputWriter.writerow(sorted(occupations))

	# write data
	for state in states:
		sortedList = getValuesSortedByKeys(statesDict[state])
		outputWriter.writerow(sortedList)







	

