var g, svgm, paths, translate, scale;

var stateName = null;

setTimeout(function() {
  svgm = d3.select("#mapDiv");
  active = d3.select(null);

  svgm.append("rect")
    .attr("class", "background")
    .attr("width", widthMap)
    .attr("height", heightMap)
    .on("click", reset);

  g = svgm.append("g")
    .style("stroke-width", "1.5px");

  paths = d3.selectAll("#usamap path");
  paths.on("click", clicked);

  dcClick = d3.select("#circle60DC");
  dcClick.on("click", clicked);


  // g.selectAll("path").attr("class", "feature")
  //                    .on("click", clicked);

  // d3.selectAll("#usamap path").on("click", clicked);
}, 100);
zoomed = false;
function clicked(d) {
  if (active.node() === this) return reset();
  if(zoomed == true){return;}
  active.classed("active", false);
  active = d3.select(this).classed("active", true);

  var stateName = this.id;
  
  var curr = d3.select(this);
  var col = d3.select(this).style("fill");

  var bounds = this.getBBox();

  var dx = (bounds.x + bounds.width) - bounds.x;  
  var dy = (bounds.y + bounds.height) - bounds.y;
  var x = (bounds.x + bounds.x + bounds.width) / 2;
  var y = (bounds.y + (bounds.y + bounds.height)) / 2;

  var onClickTransitionFinished = function() {
    console.log("on click")
    // show stateInfo display
    document.getElementById("stateInfoDiv").style.display = 'block'

  }

  if(stateName == "circle60DC") {
      curr = d3.select("#circle60DC");
      col = d3.select("#circle60DC").style("fill");
      d3.selectAll("#usamap path").on("mouseout", function() {
          d3.selectAll("#usamap path").attr("fill", "#f2f2f2");
          curr.attr("fill", col)
      }).on("mouseover", function() {
          d3.selectAll("#usamap path").attr("fill", "#f2f2f2");
          curr.attr("fill", col)
      });
      scale = .15 / Math.max(dx / widthMap, dy / heightMap);
      translate = [widthMap / 4 - scale * x, heightMap / 1.5 - scale * y];
      d3.select('#g5').transition().duration(900).attr("transform", "translate(" + translate + ")scale(" + scale + ")").each('end', onClickTransitionFinished);
  }
  else {
      d3.selectAll("#usamap path").on("mouseout", function() {
          d3.selectAll("#usamap path").attr("fill", "#f2f2f2");
          curr.attr("fill", col)
      }).on("mouseover", function() {
          d3.selectAll("#usamap path").attr("fill", "#f2f2f2");
          curr.attr("fill", col)
      });
      scale = .4 / Math.max(dx / widthMap, dy / heightMap);
      translate = [widthMap / 4 - scale * x, heightMap / 1.5 - scale * y];
      d3.select('#g5').transition().duration(900).attr("transform", "translate(" + translate + ")scale(" + scale + ")").each('end', onClickTransitionFinished);
      zoomed = true;
  }

  // make pie ring
  stateName = this.id;
  console.log("stateName");
  makePieRing(stateName);
  document.getElementById("stateZoom").style.display = 'block';

}


function reset() {
  active.classed("active", false);
  active = d3.select(null);

  var offClickTransitionFinished = function() {
    console.log('off click')
        // reset pie graph info
    var stateName = null;
    document.getElementById("stateZoom").style.display = "none";

    // reset state info
    document.getElementById("stateInfoDiv").style.display = "none";
  }

  d3.select("#g5").transition().duration(900)
      .style("stroke-width", "1.5px")
      .attr("transform", "translate(" + [-50,-10] + ")scale(" + .8 + ")")
      .each('start', offClickTransitionFinished);

  var year = document.getElementById("yslider").value;
  color(year);
  zoomed = false;




}
	