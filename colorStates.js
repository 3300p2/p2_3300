function color(year) {
    var gradientColorsD = ["#e6f7ff", "#003399"];
    var gradientColorsR = ["#990000", "#fbeaea"];
    var donations = [];

    if(year == 2008) {
        donations = donation2008;
    }
    else if (year == 2012) {
        donations = donation2012;
    }
    var gradientScaleR = d3.scale.linear().domain([-100.0, 0.0]).range(gradientColorsR);
    var gradientScaleD = d3.scale.linear().domain([0.0, 100.0]).range(gradientColorsD);

/**
  Fill all states with appropriate color. If it's DC, we must fill the circle, not the path
*/
    setTimeout(function() {
        d3.selectAll('#usamap path').attr('fill', function(d, i) {
            var abbr = this.id;
            if(donations[statesMappings[abbr]][2] < 0.0) {
               return gradientScaleR(donations[statesMappings[abbr]][2]);
            }
            else if (donations[statesMappings[abbr]][2] == 0.0) {
               return "white";
            }
            else {
                return gradientScaleD(donations[statesMappings[abbr]][2]);
            }
        })
    }, 100);


    setTimeout(function() {
        $('#circle60DC').attr('fill', function(d, i) {
            if(donations[statesMappings['DC']][2] < 0.0) {
               return gradientScaleR(donations[statesMappings[abbr]][2]);
            }
            else if (donations[statesMappings['DC']][2] == 0.0) {
               return "white";
            }
            else {
                return gradientScaleD(donations[statesMappings['DC']][2]);
            }
        })
    }, 100);  


//----------------------------------------------------------------------------------------
    /**
    Upon mousing over, fill only the state in question. If it's DC, we have 
    to fill the circle, not a path
    */
    setTimeout(function() {
        d3.selectAll('#usamap path').on('mouseover', function(d, i) {

            d3.selectAll("#usamap path, #DC").attr('fill', '#f2f2f2');
            $("#path58 > circle").attr('fill', '#f2f2f2');

            var currentState =  d3.select(this);
            var abbr = this.id;

            // console.log(this.id);
              if(donations[statesMappings[abbr]][2] < 0.0) {
                currentState.attr('fill', function() {
                    return gradientScaleR(donations[statesMappings[abbr]][2]);
                })
              }
              else if (donations[statesMappings[abbr]][2] == 0.0) {
                 currentState.attr('fill', function() {
                    return "white" 
                });
              }
              else {
                  currentState.attr('fill', function() {
                    return gradientScaleD(donations[statesMappings[abbr]][2]);
                  })
              }
        })
    }, 100);


     setTimeout(function() {
        d3.selectAll('#circle60DC').on('mouseover', function(d, i) {

            d3.selectAll("#usamap path").attr('fill', '#f2f2f2');
              if(donations[statesMappings['DC']][2] < 0.0) {
                $("#circle60DC").attr('fill', function() {
                    return gradientScaleR(donations[statesMappings['DC']][2]);
                })
              }
              else if (donations[statesMappings['DC']][2] == 0.0) {
                 $("#circle60DC").attr('fill', function() {
                    return "white" 
                });
              }
              else {
                  $("#circle60DC").attr('fill', function() {
                    return gradientScaleD(donations[statesMappings['DC']][2]);
                  })
              }
        })
    }, 100);

//----------------------------------------------------------------------------------------

/**
  Fill all states back to original color on a mouseout. If it's DC, we fill the circle, not the path
*/
    setTimeout(function() {
        d3.selectAll('#usamap path').on("mouseout", function() {

          d3.selectAll('#usamap path').attr('fill', function(d, i) {
              var abbr = this.id;

              if(donations[statesMappings[abbr]][2] < 0.0) {
                 return gradientScaleR(donations[statesMappings[abbr]][2]);
              }
              else if (donations[statesMappings[abbr]][2] == 0.0) {
                 return "white";
              }
              else {
                  return gradientScaleD(donations[statesMappings[abbr]][2]);
              }
          })

          $('#circle60DC').attr('fill', function(d) {
              if(donations[statesMappings['DC']][2] < 0.0) {
                  return gradientScaleR(donations[statesMappings['DC']][2]);
              }
              else if (donations[statesMappings['DC']][2] == 0.0) {
                  return "white" 
              }
              else {
                  return gradientScaleD(donations[statesMappings['DC']][2]);
              }
          })
        })
    }, 100);

}